<?php
class Rooms extends CI_Controller
{

 public function Rooms()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->path = realpath(APPPATH . '../images');
			$this->gallery_path_url = base_url().'images/';
			$this->logopath = realpath(APPPATH . '../');
		$this->load->library('DX_Auth');  	
		
		$this->load->model('Users_model'); 
		$this->load->model('Email_model');
		$this->load->model('Message_model');
		$this->load->model('Trips_model');
		$this->load->model('Rooms_model');
		
		$this->facebook_lib->enable_debug(TRUE);
		$this->load->library('image_lib');
		$this->path = realpath(APPPATH . '../images');
		}

	public function add()
	{
		extract($this->input->get());
		$data['user_id']   		= $user_id;
		$data['address']   		= $address;
					$level = explode(',', $data['address']);
        $keys = array_keys($level);
        $country = $level[end($keys)];
        if(is_numeric($country) || ctype_alnum($country))
        $country = $level[$keys[count($keys)-1]];
        if(is_numeric($country) || ctype_alnum($country))
        $country = $level[$keys[count($keys)-1]];
        $data['country'] = trim($country);
		
		/*$state = $level[end($keys)-1];
        if(is_numeric($state) || ctype_alnum($state))
        $state = $level[$keys[count($keys)-2]];
        $data['state'] = trim($state);
		
		$city = $level[end($keys)-2];
        if(is_numeric($city) || ctype_alnum($city))
        $city = $level[$keys[count($keys)-1]];
        $data['city'] = trim($city);*/
		$data['street_address']   		= $data['address'];
		$data['lat'] 								= $this->input->get("latitude");
		$data['long'] 							= $this->input->get("langtitude");
		$property_type = $this->input->get('property_id');  // it's a property type
		$data['property_id'] = $this->db->get_where('property_type', array('type' => $property_type))->row()->id;
		$data['room_type'] 		= $room_type;
		$data['bedrooms']			 = $bedrooms;
		$data['beds']			 = $beds;
		$data['amenities']			 = $this->input->get("amenities");
		$data['bathrooms']			 = $bathrooms;
		$data['bed_type']			 = $this->input->get("bed_type");
		$data['status']			 = '0';
	
		$data['capacity'] 			= $capacity;
		//$data['imageurl'] 			= $this->input->get('imageurl');
		$data['phone'] 						= $this->input->get("phone");
	    $data['list_pay']   = 0;
		$data['is_enable']   = 0;
		$data['cancellation_policy'] = $this->input->get('cancellation_policy'); 
 		
		$amenities = $this->input->get('amenities');
		
	 	$in_arr = explode(',', $amenities);

	    $result = $this->db->get('amnities');
		
	    $amenities_id  = '';
		
		if($result->num_rows() != 0) 
		{
	       if($amenities)
			   {
			    foreach($result->result() as $row)
	              {
	                 if(in_array($row->name, $in_arr))
						{
							$json[] = $row->id.",";
						}
				  }
				  
		$count = count($json);
		$end = $count-1;
		$slice = array_slice($json,0,$end);
		
			$comma = end($json);
			$json = substr_replace($comma ,"",-1);
			$amenities_id = $json;
         $row1 = '';
	  foreach($slice as $row)
		{
			$row1 .= $row; 
		}
			   }
			   }
	else {
		$amenities_id ='';
		}
	
	if($amenities)
			   {
		$amenities_id = $row1.$amenities_id;
       }
		
	    $data['amenities'] = $amenities_id;
		if($data['user_id'] == ''){
		  echo '[{"status":"You should provide userid."}]';exit;
		}
		else{
		$this->db->insert('list', $data);
		$insert_id = $this->db->insert_id();
		//Getting the info just entered
		$this->db->where('id',$insert_id);
		//$this->db->where('title',$data['title']);
		//$this->db->where('desc',$data['desc']);
		//$this->db->where('imageurl',$data['imageurl']);
		
		$query  = $this->db->get('list');
		
		$result = $query->result();
		
		//$data2['id']       = $result[0]->id;
		/*$data2['id']       = $insert_id;
		$data2['night']    = $data['price'];
		$data2['currency'] = $data['currency'];
		$this->db->insert('price', $data2);*/
		$lys_status['id'] = $result[0]->id;
		$lys_status['user_id'] = $data['user_id'];
		$lys_status['calendar'] = '1';
		$lys_status['price'] = '1';
		$lys_status['overview'] = '1';
		$lys_status['title'] = '1';
		$lys_status['summary'] = '1';
		//$lys_status['imageurl'] = '1';
		$lys_status['amenities'] = '1';
		$lys_status['address'] = '1';
		$lys_status['listing'] = '1';
		$lys_status['beds'] = '1';
		$lys_status['bathrooms'] = '1';
		$lys_status['photo'] = '0';
		//$lys_status['bed_type'] = '1';
		//$lys_status['bathrooms'] = '1';
		
		$this->db->insert('lys_status',$lys_status);
		//print_r($this->db->last_query());exit;
		echo '[{"reason_message":"List added successfully.", "room_id":'.$insert_id.'}]';exit;
		}	
	}
	public function photo_upload()
	{
		    $status = "";
			$msg = "";
			$file_element_name = 'uploadedfile';
			
			$this->load->model('common_model');
			
			if ($status != "error")	
			{
			
			$post_id = $this->common_model->list_post_id();
				
			$config['upload_path'] = dirname($_SERVER['SCRIPT_FILENAME']).'/images'; //Set the upload path
			
			$config['allowed_types'] = 'gif|jpg|png|jpeg'; // Set image type
			
			$config['encrypt_name']	= TRUE; // Change image name
			
			$this->load->library('upload', $config);
			
			$this->upload->initialize($config);
			
			if(!$this->upload->do_upload($file_element_name))
			{
				$status = 'error';
				$msg = $this->upload->display_errors('','');
				$data = "";
				
				echo '[{"status":"'.$msg.'"}]'; 
			}
			else 
			{
				$data = $this->upload->data(); // Get the uploaded file information
				
                $image = base_url().'images/'.$data['raw_name'].$data['file_ext'];   
							
				$config1['image_library'] = 'gd2';
				$config1['source_image'] = dirname($_SERVER['SCRIPT_FILENAME']).'/images/'.$data['raw_name'].$data['file_ext'];
				$config1['new_image'] = dirname($_SERVER['SCRIPT_FILENAME']).'/images/'.$data['raw_name'].'_100_100'.$data['file_ext'];
				//$config1['create_thumb'] = TRUE;
				$config1['maintain_ratio'] = TRUE;
				$config1['width'] = 100;
				$config1['height'] = 100;

				$this->load->library('image_lib');
				
				$this->image_lib->initialize($config1);

				if ( ! $this->image_lib->resize())
				{
   				 $resize = $this->image_lib->display_errors();
				}
				
				/*$resize = base_url().'images/'.$data['raw_name'].'_100_100'.$data['file_ext'];
				
				$config2['image_library'] = 'gd2';
				$config2['source_image'] = dirname($_SERVER['SCRIPT_FILENAME']).'/images/'.$data['raw_name'].$data['file_ext'];
				$config2['new_image'] = dirname($_SERVER['SCRIPT_FILENAME']).'/images/'.$data['raw_name'].'_320_320'.$data['file_ext'];
				//$config1['create_thumb'] = TRUE;
				$config2['maintain_ratio'] = TRUE;
				$config2['width'] = 320;
				$config2['height'] = 320;

				$this->image_lib->initialize($config2);

				if ( ! $this->image_lib->resize())
				{
   				 $resize1 = $this->image_lib->display_errors();
				}
				
				$resize1 = base_url().'images/'.$data['raw_name'].'_320_320'.$data['file_ext'];*/
		        
				echo '[{"photo":"'.$image.'"}]';exit;				
			}
					
			@unlink($_FILES[$file_element_name]);
	        }
    }
public function edit_list()
	{
		$roomid=$this->input->get('roomid');
		$data1['list_id'] 						= $this->input->get("roomid");
		$data1['created']  = time();
		$data['title'] 						= $this->input->get("title");
		$data['desc'] 					= $this->input->get("desc");
		$data['price'] 						= $this->input->get("price");
		$data['currency'] 						= $this->input->get("currency");
		$data1['name'] 						= $this->input->get("photo");
		$data['address']   		= $this->input->get("address");
		$data['city']           = $this->input->get('city');
		$data['state']        = $this->input->get('state');
		$level = explode(',', $data['address']);
        $keys = array_keys($level);
        $country = $level[end($keys)];
        if(is_numeric($country) || ctype_alnum($country))
        $country = $level[$keys[count($keys)-1]];
        if(is_numeric($country) || ctype_alnum($country))
        $country = $level[$keys[count($keys)-1]];
        $data['country'] = trim($country);
		$this->db->where('list_id',$roomid)->update('list_photo',$data1);
		$this->db->where('id',$roomid)->update('list',$data);
		echo '[{"reason_message":"Updated Successfully"}]';
	}

public function img_upload()
	{
		
			$status = "";
			$msg = "";
			$file_element_name = 'uploadedfile';
			$room_id = $this->input->get('room_id');
			 $this->path     = realpath(APPPATH . '../images');
			if ($status != "error")	
			{			
		$config['upload_path'] = dirname($_SERVER['SCRIPT_FILENAME']).'/images/'.$room_id.'/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['encrypt_name']	= TRUE; 
				
				$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if(!$this->upload->do_upload($file_element_name)){
						$status = 'error';
						$msg = $this->upload->display_errors('','');
						$data = "";
					}
					else {
							
						$data = $this->upload->data();
						
						$this->load->library('image_lib');
						
						$config['image_library'] = 'gd2';
						$config['source_image'] = dirname($_SERVER['SCRIPT_FILENAME']).'/images/'.$room_id.'/'.$data['raw_name'].$data['file_ext'];
						$config['new_image']    = 'images/'.$room_id.'/'.$data['raw_name'].$data['file_ext'];
						$config['create_thumb'] = FALSE;
						$config['maintain_ratio'] = TRUE;
						
						$config['width']     = 125;
    					$config['height']   = 125;
						
    				   $this->image_lib->initialize($config);
    				   $this->image_lib->resize();
					   
						echo $this->config->item('base_url').'images/'.$room_id.'/'.$data['raw_name'].$data['file_ext'];
						
					}
					
				@unlink($_FILES[$file_element_name]);
			}
			
	}
public function image_upload()
    {
        $id = $this->input->get('roomid');
		
        $file_element_name = 'uploadedfile';
		
        $this->path     = realpath(APPPATH . '../images');
               $status = "";
            $msg = "";
            $file_element_name = 'uploadedfile';
            
            if ($status != "error")    
            {
                if(!is_dir($this->path.'/'.$id))
            {
                    mkdir($this->path.'/'.$id, 0777, true);
                    
            }
            $config['upload_path'] = dirname($_SERVER['SCRIPT_FILENAME']).'/images/'.$id;
       
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['file_name'] = 'pic.jpg';
                $config['overwrite'] = TRUE;
                $this->load->library('upload', $config);
              
                    $this->upload->initialize($config);
            if(!$this->upload->do_upload($file_element_name)){
                        
                        $status = 'error';  
                        $msg = $this->upload->display_errors('','');
                        $data = "";
                                        echo '[{"status":"'.$msg.'"}]'; 
                        
                    }
                    else {
                    	                $data = $this->upload->data();
                $image = base_url().'/images/'.$id.'/pic_thumb.jpg';
                $config1['image_library'] = 'gd2';
                $config1['source_image'] = dirname($_SERVER['SCRIPT_FILENAME']).'/images/'.$id.'/pic.jpg';
                $config1['new_image'] = dirname($_SERVER['SCRIPT_FILENAME']).'/images/'.$id.'/pic_thumb.jpg';
                $config1['maintain_ratio'] = TRUE;
                $config1['width'] = 107;
                $config1['height'] = 78;

                $this->load->library('image_lib');
                
                $this->image_lib->initialize($config1);

                if ( ! $this->image_lib->resize())
                {
                    $resize = $this->image_lib->display_errors();
                }
                $image = base_url().'/images/'.$id.'/pic_file.jpg'; 
                $config2['image_library'] = 'gd2';
                $config2['source_image'] = dirname($_SERVER['SCRIPT_FILENAME']).'/images/'.$id.'/pic.jpg';
                $config2['new_image'] = dirname($_SERVER['SCRIPT_FILENAME']).'/images/'.$id.'/pic_file.jpg';
                $config2['maintain_ratio'] = TRUE;
                $config2['width'] = 209;
                $config2['height'] = 209;

                $this->load->library('image_lib');
                
                $this->image_lib->initialize($config2);

                if ( ! $this->image_lib->resize())
                {
                    $resize = $this->image_lib->display_errors();
                }
                $image = base_url().'/images/'.$id.'/pic_home.jpg'; 
                $config3['image_library'] = 'gd2';
                $config3['source_image'] = dirname($_SERVER['SCRIPT_FILENAME']).'/images/'.$id.'/pic.jpg';
                $config3['new_image'] = dirname($_SERVER['SCRIPT_FILENAME']).'/images/'.$id.'/pic_home.jpg';
                $config3['maintain_ratio'] = TRUE;
                $config3['width'] = 40;
                $config3['height'] = 40;

                $this->load->library('image_lib');
                
                $this->image_lib->initialize($config3);

                if ( ! $this->image_lib->resize())
                {
                    $resize = $this->image_lib->display_errors();
                }
                        $data = $this->upload->data();
                       
                        
                        $resize = base_url().'images/'.$id.'/pic.jpg';   
    
                echo '[{"image":"'.$image.'","resize":"'.$resize.'"}]';exit;
                        
                    }
                    
                @unlink($_FILES[$file_element_name]);
            }
    }
	
	
public function add_optional()
	{
	$roomid=$this->input->get('roomid');
	//$data1['id'] 						= $this->input->get("roomid");
	//$data1['week'] = $this->input->get('week');
	//$data1['month'] = $this->input->get('month');
	//$data['amenities']			 = $this->input->get("amenities");
	//$data1['currency'] 			= trim($this->input->get('currency'));	
	$amenities = $this->input->get('amenities');
		
	 	$in_arr = explode(',', $amenities);

//print_r($in_arr);exit;
	    $result = $this->db->get('amnities');
		
		//$json= array();
	    $amenities_id  = '';
		
		if($result->num_rows() != 0) 
		{
	       if($amenities)
			   {
			    foreach($result->result() as $row)
	              {
	       
	                 if(in_array(trim($row->name), $in_arr))
						{   
							$json[] = $row->id.",";
						}
						
				  }
	
				
		$count = count($json);
		$end = $count-1;
		$slice = array_slice($json,0,$end);
		
			$comma = end($json);
			$json = substr_replace($comma ,"",-1);
			$amenities_id = $json;
         $row1 = '';
	  foreach($slice as $row)
		{
			$row1 .= $row; 
		}
			   }
			   }
	else {
		$amenities_id ='';
		}
	
	if($amenities)
			   {
		$amenities_id = $row1.$amenities_id;
       }
		
		$data2['amenities'] = $amenities_id;
		$query  = $this->db->get('list');
		
		$result = $query->result();
		$lys_status['amenities'] = '1';
		$this->db->insert('lys_status',$lys_status);
		$this->db->where('id',$roomid)->update('list',$data2);
		//print_r($this->db->last_query());exit;
		echo '[{"reason_message":"Updated Successfully."}]';exit;	
	
	}
public function add_price()
{
//$roomid=$this->input->get('roomid');
	$this->load->model('Common_model');
 	$roomid	= $this->input->get("roomid");
	$week = $this->input->get('week');
	$month = $this->input->get('month');
	$currency = trim($this->input->get('currency'));
	$data1['id']	= $roomid;
	$data1['week']  = $week;
	$data1['month'] = $month;
	$data1['currency'] = $currency;
	$price_id = $this->Common_model->check_price_id($roomid);
	if(!$price_id)
		{
			$this->db->set('week',$week)->where('id',$roomid)->update('price');
			$this->db->set('month',$month)->where('id',$roomid)->update('price');
			$this->db->set('currency',$currency)->where('id',$roomid)->update('price');
			$result = $this->Common_model->get_data1('price', array('id' => $roomid));
		
			$row = $result->row();
			
			echo '[{"status":"Updated Successfully","room_id":"'.$row->id.'","week":"'.$row->week.'","month":"'.$row->month.'","currency":"'.$row->currency.'"}]';
		}
else
	{
	$this->db->where('id',$roomid)->insert('price',$data1);
	echo '[{"reason_message":"Successfully Added."}]';exit;
	}	
}
public function add_description()
{
		$roomid=$this->input->get('roomid');
	
		$data['space'] 		= $this->input->get("space");
		$data['guests_info']			 = $this->input->get("guests_info");
		$data['interaction']			 = $this->input->get("interaction");
		$data['overview']			 = $this->input->get("overview");
		$data['getting_around']			 = $this->input->get("getting_around");
		$data['other_thing']			 = $this->input->get("other_thing");
		$data['house_rule']			 = $this->input->get("house_rule");
		
		$this->db->where('id',$roomid)->update('list',$data);
		
		//print_r($this->db->last_query());
		echo '[{"reason_message":"Description added Successfully"}]';
	 
}
public function edit_room()
{
	$this->load->model('common_model');
		$roomid=$this->input->get('roomid');
	
		$data['beds']			 = $this->input->get("beds");
		$data['bedrooms']			 = $this->input->get("bedrooms");
		$data['bathrooms']			 = $this->input->get("bathrooms");
		$data['capacity']			 = $this->input->get("guest");
		$data['room_type']			 = $this->input->get("room_type");
		$data['home_type']			 = $this->input->get("home_type");
		
		$this->db->where('id',$roomid)->update('list',$data);
		
		/*$result = $this->common_model->get_data('list',$data);
		$row = $result->row();
		
	echo '[{"status":"Updated Successfully","user_id":"'.$row->user_id.'","room_id":"'.$row->id.'","beds":"'.$row->beds.'","bedrooms":"'.$row->bedrooms.'","bathrooms":"'.$row->bathrooms.'","guest":"'.$row->capacity.'","room_type":"'.$row->room_type.'","home_type":"'.$row->home_type.'"}]';
			exit;*/
		
		echo '[{"reason_message":"Updated Successfully"}]';
	 
}
public function view_room()
{
		$roomid=$this->input->get('roomid');
	
		$result = $this->db->where('id',$roomid)->get('list');
		$row = $result->row();
		echo '[{"status":"Edit Your Details","user_id":"'.$row->user_id.'","room_id":"'.$row->id.'","beds":"'.$row->beds.'","bedrooms":"'.$row->bedrooms.'","amenities":"'.$row->amenities.'","bathrooms":"'.$row->bathrooms.'","guest":"'.$row->capacity.'","room_type":"'.$row->room_type.'","home_type":"'.$row->home_type.'"}]';
			exit;
		
}
public function view_price()
{
		$roomid=$this->input->get('roomid');
	
		$result = $this->db->where('id',$roomid)->get('price');
		$row = $result->row();
		echo '[{"status":"Edit Your Details","room_id":"'.$row->id.'","month":"'.$row->month.'","week":"'.$row->week.'","currency":"'.$row->currency.'"}]';
			exit;
		
}
public function view_listing()
{
		$roomid=$this->input->get('roomid');
		//$user_id=$this->input->get('user_id');
		$final_value = $this->db->where('id',$roomid)->get('list');
		$image_list = $this->db->where('list_id',$roomid)->get('list_photo');
		if($final_value->num_rows() !=0)
	{
		foreach ($final_value->result() as $row) 
		{
			$data['id'] = $row->id;
			$data['address'] = $row->address;
			$data['currency'] = $row->currency;
			//$data['user_id'] = $row->user_id;
			$data['price'] = $row->price;
			$data['desc'] = $row->desc;
			if(!empty($image_list->row()->image))
			{
			$data['image'] = $image_list->row()->image;
			}
			else {
				$data['image'] = '';
			}
			$data['title'] = $row->title;
			
			$history[]  = $data;
			
		}
		echo json_encode($history);
		}
		else {
			echo '[{"status":"No Data Found"}]';
		}
		
}
	public function add_room()
	{
		$roomid=$this->input->get('roomid');
$data1['list_id'] 						= $this->input->get("roomid");
$data1['created']  = time();
$data1['highlights'] 						= $this->input->get("highlights");
			 					
			//$data['status'] 						= $this->input->get("status");
			
			
			$title_status	= $this->input->get("title");
			
			if(!empty($title_status))
			{
			
			$data5['title'] = 1 ;
			}
			else {
				
				$data5['title'] = 0;
			}
			$data['title'] = $title_status;
			$summary_status				= $this->input->get("description");
			
			if(!empty($summary_status))
			{
			
			$data5['summary'] = 1 ;
			}
			else {
				
				$data5['summary'] = 0;
			}
		$data['desc'] = $summary_status;
		$price_status 						= $this->input->get("price");
		
		if(!empty($price_status))
			{
			
			$data5['price'] = 1 ;
			}
			else {
				$data5['price'] = 0;
			}
			$data['price'] = $price_status;
			$photo_status 						= $this->input->get("photo");
				
			if(!empty($photo_status))
			{
			
			$data5['photo'] = 1 ;
			}
			else {
				$data5['photo'] = 0;
			}
			$data1['name'] = $photo_status;
			$address_status   		= $this->input->get("address");
			
			if(!empty($photo_status))
			{
			
			$data5['address'] = 1 ;
			}
			else {
				$data5['address'] = 0;
			}
			$data['address'] = $address_status;
			
				
			
			
			
			
			
			
			
					$data['city']           = $this->input->get('city');
					$data['state']        = $this->input->get('state');
					$data['country']        = $this->input->get('country');
					$data['street_address']   		= $address_status;
							$data['currency'] 			= trim($this->input->get('currency'));	
					
		
		/*$level = explode(',', $data['address']);
		   $keys = array_keys($level);
        $country = $level[end($keys)];
        if(is_numeric($country) || ctype_alnum($country))
        $country = $level[$keys[count($keys)-3]];
        $data['country'] = trim($country);
		
		$state = $level[end($keys)-1];
        if(is_numeric($state) || ctype_alnum($state))
        $state = $level[$keys[count($keys)-2]];
        $data['state'] = trim($state);
		
		$city = $level[end($keys)-2];
        if(is_numeric($city) || ctype_alnum($city))
        $city = $level[$keys[count($keys)-1]];
        $data['city'] = trim($city);*/
		$data['lat'] 								= $this->input->get("latitude");
		$data['long'] 							= $this->input->get("langtitude");
		$this->db->where('list_id',$roomid)->order_by('id','asc')->insert('list_photo',$data1);
		$this->db->where('id',$roomid)->update('list_photo',$data1);
		//print_r($this->db->last_query());exit;
		$lys_stats = $this->db->where('id',$roomid)->get('lys_status');
				
				$address_status1 = $lys_stats->row()->address;
				$price_status3 = $lys_stats->row()->price;
				$photo_status2 = $lys_stats->row()->photo;
				$title_status1 = $lys_stats->row()->title;
				$summary_status4 = $lys_stats->row()->summary;
				//print_r($title_status1);exit;
			
			if(($address_status1 == 1) && ($photo_status2 == 1) && ($price_status3 == 1) && ($summary_status4 == 1) && ($title_status1 == 1))
		{
		
			$data['status']= 1 ;
			
		}
		else 
		{
				$data['status']= 0 ;
			}
		
		
		$this->db->where('id',$roomid)->update('list',$data);
		$this->db->where('id',$roomid)->update('lys_status',$data5);
		
		//print_r($this->db->last_query());
		echo '[{"reason_message":"Updated Successfully"}]';
	}
	public function update($param = '')
	{
	
	  if($param != '')
			{
			 $amenity   = $this->input->get('amenities');
				$aCount    = count($amenity);
				
				$amenities = '';	
				if(is_array($amenity))
				{
					if(count($amenity) > 0)
					{
						$i = 1;
						foreach($amenity as $value)
						{
								if($i == $aCount) $comma = ''; else $comma = ',';
								
								$amenities .= $value.$comma;
								$i++;
						}
					}
			 }
				
				$updateData = array(
						'property_id'  					=> $this->input->get('property_id'),
						'room_type'   		     		 => $this->input->get('room_type'),
						'title'    						 => $this->input->get('hosting_descriptions'),
						'desc'         					=> $this->input->get('desc'),
						'capacity'     					=> $this->input->get('capacity'),
						'bedrooms'    	     			 => $this->input->get('bedrooms'),
						'beds'    						=> $this->input->get('beds'),
						'bed_type'     					=> $this->input->get('hosting_bed_type'),
						'bathrooms'     				=> $this->input->get('hosting_bathrooms'),
						'manual'     					=> $this->input->get('manual'),
					    'street_view'     				=> 0,
				     	'directions'     		     	=> $this->input->get('hosting_directions')
																	);
																	
				if(isset($_POST['address']['formatted_address_native']))
				{												
					$address = $_POST['address']['formatted_address_native'];
					if(!empty($address))
					{
					$address = urlencode($address);
					$address = str_replace('+','%20',$address); 
					$geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$address.'&sensor=false');
					$output= json_decode($geocode);
					
					$updateData['address'] = $_POST['address']['formatted_address_native'];
					
					$level = explode(',', $updateData['address']);
        $keys = array_keys($level);
        $country = $level[end($keys)];
        if(is_numeric($country) || ctype_alnum($country))
        $country = $level[$keys[count($keys)-1]];
        if(is_numeric($country) || ctype_alnum($country))
        $country = $level[$keys[count($keys)-1]];
        $updateData['country'] = trim($country);
					
					$updateData['lat'] 				= $output->results[0]->geometry->location->lat;
					$updateData['long'] 			= $output->results[0]->geometry->location->lng;
					}
				}
																	
			if($amenities != '')
			{
			$updateData['amenities'] = $amenities;
			}
																	
			$updateKey = array('id' => $param);									
		 $this->Rooms_model->update_list($updateKey, $updateData);
			
		 //echo $this->db->last_query();exit;
																
			echo '{"redirect_to":"'.base_url().'rooms/'.$param.'","result":"success"}';
			
			}
	}
	
		public function edit_photo($param  = '')
	{

				$this->load->model('Gallery');
				$listId           = $param;
				$images           = $this->input->get('image');
				if(!empty($images))
				{
					foreach($images as $image)
					{
							unlink($image);
					}
				}
		
					if(isset($_FILES["userfile"]["name"]))
					{
						if(!is_dir($this->path.'/'.$listId))
						{
							//echo $this->path.'/'.$id;
							mkdir($this->path.'/'.$listId, 0777, true);
						}
						$config = array(
							'allowed_types' => 'jpg|jpeg|gif|png',
							'upload_path' => $this->path.'/'.$listId
						);
						//echo $this->path.'/'.$id;
						$this->load->library('upload', $config);
						$this->upload->do_upload();
					}
					
					$rimages = $this->Gallery->get_images($listId);
					$i = 1;
					$replace = '<ul class="clearfix">';
					foreach ($rimages as $rimage)
					{		
						$replace .= '<li><p><label><input type="checkbox" name="image[]" value="'.$rimage['path'].'" /></label>';
						$replace .= '<img src="'.$rimage['url'].'" width="150" height="150" /></p></li>';
								$i++;
					}
					$replace .= '</ul>';
					
					echo $replace;
		
	}
	
	
		public function update_price()
	{
				$data = array(
				'currency' 	=> $this->input->get('currency'),
				'night' 	=> $this->input->get('nightly'),
				'week' 		=> $this->input->get('weekly'),
				'month' 	=> $this->input->get('monthly'),
				'addguests' => $this->input->get('extra'),
				'guests'    => $this->input->get('guests'),
				'security' 	=> $this->input->get('security'),
				'cleaning' 	=> $this->input->get('cleaning')
				);

			$this->db->where('id', $this->uri->segment(3));
			$this->db->update('price', $data);
			
		redirect ('rooms/edit_price/'.$this->uri->segment(3),'refresh'); 
	}
	
	
	public function edit_price()
	{
		if( ($this->dx_auth->is_logged_in()) || ($this->facebook_lib->logged_in()))
		{	
			$data['title'] = "Edit the price information for your site";
			$data['message_element'] = 'rooms/view_edit_price';
			$this->load->view('template',$data);
		}
		else
		{
			redirect('users/signin');
		}
	}
	
		
	public function change_status()
	{
			$sow_hide = $this->input->get('stat'); 
			$row_id   = $this->input->get('rid');
			
			if($sow_hide == 1)
			{
				$data['status'] = 0;
				$this->db->where('id',$row_id);
				$this->db->update('list',$data);
				redirect('hosting');
			}
			else
			{
				$data['status'] = 1;
				$this->db->where('id',$row_id);
				$this->db->update('list',$data);
				redirect('hosting');
			}	
 }
		
	public function change_availability($param = '')
	{
	if($param != '')
	{ 
	 $is_available = $this->input->post('is_available');
	 if($is_available == 0)
		{
  	echo '{"result":"unavailable","message":"Your listing will be hidden from public search results.","available":false,"prompt":"Your listing can now be activated!"}';
		}
		else
		{	
   echo	'{"result":"available","message":"Your listing will now appear in public search results.","available":true,"prompt":"Your listing is active."}';
		}
	}
		
	}

public function recent_view()
    {
		//$conditions     = array("list.is_enable" => 1, "list.status" => 1);
//$limit          = array(12);
//$orderby        = array("page_viewed", "desc");
//$mosts  = $this->Rooms_model->get_rooms($conditions, NULL, $limit, $orderby);
$mosts = $this->db->select('*')->order_by('page_viewed','desc')->limit('12')->from('list')->get();
if($mosts->num_rows() != 0)
	{
		echo "[ ";
	foreach($mosts->result() as $row)
	{
	
              $image_query = $this->db->select('name')->where('list_id',$row->id)->where('is_featured',1)->from('list_photo')->get();
			if($image_query->num_rows() != 0)
			{
			foreach($image_query->result() as $rows)
			{
				$image_name = $rows->name;
		    }
			$images = base_url().'images/'.$row->id.'/'.$image_name;
			}
			else
				{
					$images=base_url().'images/no_image.jpg';
					
			    }
	       //$json[] = "{ \"id\":".$id.",\"title\":\"".$row->title."\",\"country\":\"".$country."\",\"image_url\":\"".$image."\" },";
		   $json[] = "{ \"id\":".$row->id.",\"title\":\"".$row->title."\",\"country\":\"".$row->country."\",\"image_url\":\"".base_url()."files/timthumb.php?src=".$images."&h=309&w=598&zc=&q=100\",\"address\":\"".
		   $row->address."\",\"price\":\"".$row->price."\"},";
		 
	}
	      $count = count($json);
		  $end = $count-1;
					$slice = array_slice($json,0,$end);
					foreach($slice as $row)
					{
						echo $row;
					}
					$comma = end($json);
					$json = substr_replace($comma ,"",-1);
					echo $json;
					echo " ]";
				
	}
	}

	public function neighborhoods()
    {
	$conditions     = array("list.is_enable" => 1, "list.status" => 1, "list.list_pay" => 1);
	$conditions_lys_status = array("lys_status.photo" => 1, "lys_status.calendar" => 1, "lys_status.price" => 1, "lys_status.overview" => 1, "lys_status.address" => 1, "lys_status.listing" => 1);
//$limit          = array(12);
$orderby        = array("page_viewed", "desc");
$distinct = 'country';
//$mosts  = $this->Rooms_model->get_rooms($conditions, NULL, NULL, $orderby, NULL, NULL, NULL, $distinct);
$mosts = $this->db->distinct('list.country')->where($conditions)->order_by('list.id','asc')->where($conditions_lys_status)->join('lys_status','lys_status.id = list.id')->get('list');
//$items= '';
//echo $this->db->last_query();exit;
if($mosts->num_rows() != 0)
	{
		echo "[ ";
	foreach($mosts->result() as $row)
	{
		$items[] = $row->country;
		//$itemsid[] = $row->id;
		}
	//echo'<pre>';print_r(array_unique($items));exit;
	$result_id = array();
	$i=0;
	$result_country = array_unique($items);
	//print_r($result_country);exit;
	$final_country = array();
	$i=0;
	foreach($result_country as $row_country)
	{
		if($i < 7)
		{
		$final_country[] = $row_country;
		$i++;
		}
	}		
	//print_r($final_country);
				  foreach($final_country as $rows_country)
				 {
 				 	$conditions     = array("country" => $rows_country,"list.is_enable"=>1);
					//$distinct = 'country';
				 	$mosts1  = $this->Rooms_model->get_rooms($conditions, NULL, NULL, $orderby);
					foreach($mosts1->result() as $row_ids)
					{
						$list_id[] = $row_ids->id;
					}				
					$count = count($list_id);
	       //$json[] = "{ \"id\":".$id.",\"title\":\"".$row->title."\",\"country\":\"".$country."\",\"image_url\":\"".$image."\" },";
		 
		 $condition    = array("is_featured" => 1);
$list_image   = $this->Gallery->get_imagesG($list_id[$count-1], $condition)->row();

if(isset($list_image->name))
{
$image_url_ex = explode('.',$list_image->name);

$image_url = base_url().'images/'.$list_id[$count-1].'/'.$image_url_ex[0].'_crop.jpg';
}
else
{
$image_url = base_url().'images/no_image.jpg';
}
		  
		   $json[] = "{\"country\":\"".$rows_country."\",\"image_url\":\"".$image_url."\"},";
		 
	}
	      $count = count($json);
		  $end = $count-1;
					$slice = array_slice($json,0,$end);
					foreach($slice as $row)
					{
						echo $row;
					}
					$comma = end($json);
					$json = substr_replace($comma ,"",-1);
					echo $json;
					echo " ]";
				
	}
	}


 public function selected_view()
	{
		$room_id = $this->input->get('room_id');
		
     $conditions             = array("id" => $room_id, "list.is_enable" => 1, "list.status" => 1);
     $result                 = $this->Common_model->getTableData('list', $conditions);
		
		if($result->num_rows() != 0)
	{
	foreach($result->result() as $row)
	{
		$id = $row->id;
		$user_id = $row->user_id;
		$address=$row->address;
		$country='';
		$city='';
		$state='';
		$cancellation_policy = $row->cancellation_policy; 	
		$room_type=$row->room_type;
		$bedrooms=$row->bedrooms;
		$beds=$row->beds;
		$bed_type=$row->bed_type;
		$bathrooms=$row->bathrooms;
		$title=$row->title;
		$desc=$row->desc;
		$capacity=$row->capacity;  
		$price=$row->price; 
		$email=$row->email;
		$phone=$row->phone;
		$review=$row->review;
		$lat=$row->lat;
		$long=$row->long;
		$property_id=$row->property_id;
		$street_view=$row->street_view;
		$sublet_price=$row->sublet_price;
		$sublet_status=$row->sublet_status;
		$sublet_startdate=$row->sublet_startdate;
		$sublet_enddate=$row->sublet_enddate;
		$currency=$row->currency;
		$manual=$row->manual;
		$page_viewed=$row->page_viewed;
		$neighbor=$row->neighbor;
		$amenities = $row->amenities;
		$hometype=$row->home_type;
		
		$price_query=$this->db->where('id',$room_id)->from('price')->get();
		
		if($price_query->num_rows() != 0)
	   { 
		foreach($price_query->result() as $row)
	   {
	if($currency == 'USD' || $currency == '0')
			{
				$price = $price;
				$cleaning_fee = $row->cleaning;
	            $extra_guest_fee = $row->addguests.'/guest after'.$row->guests;
		        $Wprice = $row->week;
		        $Mprice = $row->month;
		    }
			else {
				
			    $params_price  = array('amount' => $price, 'currFrom' => $currency,'currInto' => 'USD');
				$params_clean  = array('amount' => $row->cleaning, 'currFrom' => $currency,'currInto' => 'USD');
				$params_guest  = array('amount' => $row->addguests, 'currFrom' => $currency,'currInto' => 'USD');
				$params_week   = array('amount' => $row->week, 'currFrom' => $currency,'currInto' => 'USD');
				$params_month  = array('amount' => $row->month, 'currFrom' => $currency,'currInto' => 'USD');
						
			$price = round(google_convert($params_price));
			$cleaning_fee = round(google_convert($params_clean));
			$extra_guest_fee = round(google_convert($params_guest));
			$Wprice = round(google_convert($params_week));
			$Mprice = round(google_convert($params_month));

			}
	   }
	   }
else
	{
		$Wprice='';
		$Mprice='';
		$cleaning_fee='';
		$price='';
		$extra_guest_fee='';
	}
			
	
     $conditions             = array("id" => $room_id, "list.is_enable" => 1, "list.status" => 1);
	 $result                 = $this->Common_model->getTableData('list', $conditions);
 	 	$today_month=date("F");
		$today_date=date("j");
		$today_year=date("Y");
		$conditions_statistics = array("list_id" => $room_id,"date"=>trim($today_date),"month"=>trim($today_month),"year"=>trim($today_year));
		$result_statistics = $this->Common_model->add_page_statistics($room_id,$conditions_statistics);
		
		$list                   = $list = $result->row();
		$title                  = $list->title;
		$page_viewed            = $list->page_viewed;
		
		$page_viewed = $this->Trips_model->update_pageViewed($room_id, $page_viewed);
		
			
		$id                     = $room_id;
		$checkin                = $this->session->userdata('Vcheckin');
		$checkout               = $this->session->userdata('Vcheckout');
		$guests         = $this->session->userdata('Vnumber_of_guests');
	
		$ckin                   = explode('/', $checkin);
		$ckout                  = explode('/', $checkout);
		
		//check admin premium condition and apply so for
		$query                  = $this->Common_model->getTableData( 'paymode', array('id' => 2));
		$row                    = $query->row();	

		
		if(($ckin[0] == "mm" && $ckout[0] == "mm") or ($ckin[0] == "" && $ckout[0] == ""))
		{
      			
			 if($Wprice == 0)
			{
				$data['Wprice']  = $price * 7;
			}
			else
			{
				$data['Wprice']  = $Wprice;
			}
			if($Mprice == 0)
			{
				$data['Mprice']  = $price * 30;
			}
			else
			{
				$data['Mprice']  = $Mprice;
			}
			
			 if($row->is_premium == 1)
					{
			    if($row->is_fixed == 1)
							{
										$fix            = $row->fixed_amount; 
										$amt            = $price + $fix;
										$commission = $fix;
										$Fprice         = $amt;
							}
							else
							{  
										$per            = $row->percentage_amount; 
										$camt           = floatval(($price * $per) / 100);
										$amt            = $price + $camt;
										$commission = $camt;
										$Fprice         = $amt;
							}
							
						if($Wprice == 0)
			{
				$data['Wprice']  = $price * 7;
			}
			else
			{
				$data['Wprice']  = $Wprice;
			}
			if($Mprice == 0)
			{
				$data['Mprice']  = $price * 30;
			}
			else
			{
				$data['Mprice']  = $Mprice;
			}
		
		   }
			} 
		else
		{	
			$diff                  = strtotime($ckout[2].'-'.$ckout[0].'-'.$ckout[1]) - strtotime($ckin[2].'-'.$ckin[0].'-'.$ckin[1]);
			$days                  = ceil($diff/(3600*24));
			
			if($guests > $guests)
			{
			  $price               = ($price * $days) + ($days * $xprice->addguests);
			}
			else
			{
			  $price               = $price * $days;
			}
					
			if($Wprice == 0)
			{
				$data['Wprice']  = $price * 7;
			}
			else
			{
				$data['Wprice']  = $Wprice;
			}
			if($Mprice == 0)
			{
				$data['Mprice']  = $price * 30;
			}
			else
			{
				$data['Mprice']  = $Mprice;
			}
			
			$commission    = 0;
			
			 if($row->is_premium == 1)
					{
			    if($row->is_fixed == 1)
							{
										$fix             = $row->fixed_amount; 
										$amt             = $price + $fix;
										$commission = $fix;
										$Fprice          = $amt;
							}
							else
							{  
										$per             = $row->percentage_amount; 
										$camt            = floatval(($price * $per) / 100);
										$amt             = $price + $camt;
										$commission = $camt;
										$Fprice          = $amt;
							}
							
						if($Wprice == 0)
			{
				$data['Wprice']  = $price * 7;
			}
			else
			{
				$data['Wprice']  = $Wprice;
			}
			if($Mprice == 0)
			{
				$data['Mprice']  = $price * 30;
			}
			else
			{
				$data['Mprice']  = $Mprice;
			}
		
		   }
					}
		
			$conditions              = array('list_id' => $room_id);
			$image_query = $this->db->select('name')->where('list_id',$room_id)->where('is_featured',1)->from('list_photo')->get();
			
			if($image_query->num_rows() != 0)
			{
			foreach($image_query->result() as $row)
			{
				$image_name = $row->name;
		    }
			$image = base_url().'images/'.$room_id.'/'.$image_name;
			}
			else
				{
			 $image=base_url().'images/no_image.jpg';
				}
			
			$conditions    			        = array('list_id' => $room_id, 'userto' => $list->user_id);
			$result			     	  = $this->Trips_model->get_review($conditions);
			
			$conditions    			     	  = array('list_id' => $room_id, 'userto' => $list->user_id);
			$stars			        	= $this->Trips_model->get_review_sum($conditions)->row();	
			 
			$title            = substr($title, 0, 70);
			
			$level = explode(',', $address);
		$keys = array_keys($level);
		$country = $level[end($keys)];
		if(is_numeric($country) || ctype_alnum($country))
		$country = $level[$keys[count($keys)-2]];
		if(is_numeric($country) || ctype_alnum($country))
		$country = $level[$keys[count($keys)-3]];
		   
		   $search=array('\'','"','(',')','!','{','[','}',']');
			$replace=array('&sq','&dq','&obr','&cbr','&ex','&obs','&oabr','&cbs','&cabr');
		    $desc_replace = str_replace($search, $replace, $desc);
			$desc_tags = stripslashes($desc_replace);
							 
			$amenities = $this->db->get_where('list', array('id' => $room_id))->row()->amenities;
				 $property_type = $this->db->get_where('property_type', array('id' => $property_id))->row()->type;
    $in_arr = explode(',', $amenities);
	$result = $this->db->get('amnities');				 
							 $user_name = $this->db->where('id',$user_id)->select('username')->from('users')->get();
							 if($user_name->num_rows()!=0)
							 {
							 	foreach($user_name->result() as $row)
								{
									$hoster_name = $row->username;
								}
							 }
							 else
							 	{
							 		$hoster_name = 'No Username';
							 	}
							
							
								
            echo "[ { \"id\":".$room_id.",\"user_id\":".$user_id.",\"hoster_name\":\"".$hoster_name."\",\"title\":\"".$title."\",\"country\":\"".$country.
			    "\",\"city\":\"".$city."\",\"state\":\"".$state."\",\"cancellation_policy\":\"".$cancellation_policy.
	           "\",\"address\":\"".$address."\",\"image_url\":\"".base_url()."files/timthumb.php?src=".$image."&h=309&w=598&zc=&q=100\",
	           \"room_type\":\"".$room_type."\",\"bedrooms\":".$bedrooms.",\"bathrooms\":".$bathrooms.",\"bed_type\":\"".$bed_type."\",
	           \"desc\":\"".$desc_tags."\",\"capacity\":".$capacity.",\"price\":\"".$price.
	           "\",\"cleaning_fee\":\"".$cleaning_fee."\",\"extra_guest_fee\":\"".$extra_guest_fee."\",\"weekly_price\":\"".$Wprice.
	           "\",\"monthly_price\":\"".$Mprice."\",\"email\":\"".$email."\",\"phone\":\"".$phone."\",\"review\":\"".$review.
	           "\",\"lat\":".$lat.",\"long\":".$long.",\"property_type\":\"".$property_type."\",\"street_view\":".$street_view.
	           ",\"sublet_price\":".$sublet_price.",\"sublet_status\":".$sublet_status.",\"sublet_startdate\":\"".$sublet_startdate.
	           "\",\"sublet_enddate\":\"".$sublet_enddate."\",\"currency\":\"".$currency."\",\"manual\":\"".$manual."\",\"page_viewed\":".$page_viewed
	           .",\"neighbor\":\"".$neighbor."\",\"amenities\":\"";if($result->num_rows() != 0) {
	           if($amenities)
			   {
			    foreach($result->result() as $row)
	{
	    if(in_array($row->id, $in_arr))
		{
			$json[] = $row->name.",";
		}
	}
	$count = count($json);
		  $end = $count-1;
					$slice = array_slice($json,0,$end);
					foreach($slice as $row)
					{
						echo $row; 
					}
					$comma = end($json);
					$json = substr_replace($comma ,"",-1);
					echo $json."\""; echo "} ]";exit;
			   }
			   }
else {
	$json[] ='';
	
}
		echo "\"} ]";			
			  
	}
	}
	
	else {
	echo "[ { \"status\":\"Access Denied\" } ]";
}
	}
 function availability()
 {
 	            $checkin = $this->input->get('checkin');
 	            $checkin_time = $checkin;
 	            
				$checkin_time=get_gmt_time(strtotime($checkin_time)); 
				
				$checkout = $this->input->get('checkout');
				$checkout_time= $checkout;
				
                $checkout_time=get_gmt_time(strtotime($checkout_time)); 
				
				$id = $this->input->get('room_id');
				
				
				 $conditions             = array("id" => $id);
                 $result                 = $this->Common_model->getTableData('list', $conditions);
				 if($result->num_rows() == 0)
	               {
	                 	echo "[ { \"status\":\"Access Denied\" } ]";
	               }
				 else{
             $status=1;	
			$daysexist = $this->db->query("SELECT id,list_id,booked_days FROM `calendar` WHERE `list_id` = '".$id."' AND (`booked_days` >= '".$checkin_time."' AND `booked_days` <= '".$checkout_time."') GROUP BY `id`");
			$rowsexist = $daysexist->num_rows();
		    if($rowsexist > 0)
			{
				$status=0;
				
			} 	
			if($status == 0)
			{
		      echo "[ { \"status\":\"NO\" } ]";
		           
		   }	
			else 
			{
				echo "[ { \"status\":\"YES\" } ]";
			}
			}
 }

function property_type()
{
	$property_type = $this->db->select('*')->from('property_type')->get();
	echo json_encode($property_type->result());
	
}

function amenities()
{
	$amenities = $this->db->select('*')->from('amnities')->get();
	echo json_encode($amenities->result());
}

function calendar()
{
	extract($this->input->get());
	
	$result = $this->db->where('calendar.list_id',$list_id)->get('calendar');
	
	if($result->num_rows() != 0)
	{
	 echo json_encode($result->result());	
	}
	else
		{
			echo "[ { \"status\":\"No data\" } ]";
		}
	
}

function seasonal_price_calendar()
{
	extract($this->input->get());
	
	$result = $this->db->where('seasonalprice.list_id',$list_id)->get('seasonalprice');
	
	if($result->num_rows() != 0)
	{
		echo '[';
		foreach($result->result() as $row)
		{
			$price = $this->get_currency_value1($row->list_id, $row->price, $currency);
			$json[] = '{"id":"'.$row->id.'","list_id":"'.$row->list_id.'","price":"'.$price.'","start_date":"'.$row->start_date.'","end_date":"'.$row->end_date.'"},';			
		}
        $count = count($json);
		  $end = $count-1;
					$slice = array_slice($json,0,$end);
					foreach($slice as $row)
					{
						echo $row; 
					}
					$comma = end($json);
					$json = substr_replace($comma ,"",-1);
					echo $json;
		echo ']';	
	}
	else
		{
			echo "[ { \"status\":\"No data\" } ]";
		}
	
}

function get_currency_value1($id,$amount,$currency)
		{
			$rate=0;
						
			$this->load->helper("cookie");
						
			$current = $currency;
			
			$list_currency     = $this->db->where('id',$id)->get('list')->row()->currency;
			
			if($current == '')
			{
				$list_currency1 = $this->db->where('default',1)->get('currency')->row()->currency_code;
				
				$params  = array('amount' => $amount, 'currFrom' => $list_currency, 'currInto' => $list_currency1);
						
			$rate=round(google_convert($params));
				
			if($rate!=0)
				return $rate;
			else
				return 0;
			
			}
			
			$params  = array('amount' => $amount, 'currFrom' => $list_currency, 'currInto' => $current);
			
			$rate=round(google_convert($params));
						
			if($rate!=0)
				return $rate;
			else
				return 0;
	}
	function google_convert($params)
	{
		$amount    = $params["amount"];
		
		$currFrom  = $params["currFrom"];
		
		$currInto  = $params["currInto"];
		
		if (trim($amount) == "" ||!is_numeric($amount)) {
			trigger_error("Please enter a valid amount",E_USER_ERROR);         	
		}
		$return=array();
			
		if($currFrom == 'USD')
		{
			$currInto_result = $this->db->where('currency_code',$currInto)->get('currency_converter')->row();
			$rate = $amount * $currInto_result->currency_value;
		}
		else 	
		{
			
		$currFrom_result = $this->db->where('currency_code',$currFrom)->get('currency_converter')->row();
		
		$from_usd = 1/$currFrom_result->currency_value; 
		
		$from_usd_amt = $amount * $from_usd;
		
		$currInto_result = $this->db->where('currency_code',$currInto)->get('currency_converter')->row();
		
		$rate = $currInto_result->currency_value * $from_usd_amt;
		
		}
		
		return $rate;
	}

function edit_listing()
{
	extract($this->input->get());
	
	$check_list = $this->db->where('id',$list_id)->where('user_id',$user_id)->get('list');
	
	$room_id = $list_id;
	
	if($check_list->num_rows() != 0)
	{
		foreach($check_list->result() as $row)
		{
		$id = $row->id;
		$user_id = $row->user_id;
		$address=$row->address;
		$country=$row->country;
		$city=$row->city;
		$state=$row->state;
		$cancellation_policy = $row->cancellation_policy; 	
		$room_type=$row->room_type;
		$bedrooms=$row->bedrooms;
		$beds=$row->beds;
		$bed_type=$row->bed_type;
		
		if($row->bathrooms == NULL)
		{
			$bathrooms = '""';
		}
		else
		$bathrooms=$row->bathrooms;
		
		$title=$row->title;
		$desc=$row->desc;
		$capacity=$row->capacity;  
		$price=$row->price; 
		$email=$row->email;
		$phone=$row->phone;
		$review=$row->review;
		$lat=$row->lat;
		$long=$row->long;
		$property_id=$row->property_id;
		$street_view=$row->street_view;
		$sublet_price=$row->sublet_price;
		$sublet_status=$row->sublet_status;
		$sublet_startdate=$row->sublet_startdate;
		$sublet_enddate=$row->sublet_enddate;
		$currency=$row->currency;
		$manual=$row->house_rule;
		
		$page_viewed=$row->page_viewed;
		$neighbor=$row->neighbor;
		$amenities = $row->amenities;
		
		$price_query=$this->db->where('id',$room_id)->from('price')->get();
		
		if($price_query->num_rows() != 0)
	   { 
		foreach($price_query->result() as $row)
	   {
	//if($currency == 'USD' || $currency == '0')
			//{
				$price = $price;
				$cleaning_fee = $row->cleaning;
	            $extra_guest_fee = $row->addguests.'/guest after'.$row->guests;
		        $Wprice = $row->week;
		        $Mprice = $row->month;
		    //}
			/*else {
				
			    $params_price  = array('amount' => $price, 'currFrom' => $currency,'currInto' => 'USD');
				$params_clean  = array('amount' => $row->cleaning, 'currFrom' => $currency,'currInto' => 'USD');
				$params_guest  = array('amount' => $row->addguests, 'currFrom' => $currency,'currInto' => 'USD');
				$params_week   = array('amount' => $row->week, 'currFrom' => $currency,'currInto' => 'USD');
				$params_month  = array('amount' => $row->month, 'currFrom' => $currency,'currInto' => 'USD');
						
			$price = round(google_convert($params_price));
			$cleaning_fee = round(google_convert($params_clean));
			$extra_guest_fee = round(google_convert($params_guest));
			$Wprice = round(google_convert($params_week));
			$Mprice = round(google_convert($params_month));

			}*/
			$after_guest_fee = $row->addguests;
	   }
	   }
else
	{
		$Wprice='';
		$Mprice='';
		$cleaning_fee='';
		$price='';
		$extra_guest_fee='';
	}
			
	
     $conditions             = array("id" => $room_id);
	 $result                 = $this->Common_model->getTableData('list', $conditions);
	 
	 	$today_month=date("F");
		$today_date=date("j");
		$today_year=date("Y");
		$conditions_statistics = array("list_id" => $room_id,"date"=>trim($today_date),"month"=>trim($today_month),"year"=>trim($today_year));
		$result_statistics = $this->Common_model->add_page_statistics($room_id,$conditions_statistics);
		
		$list                   = $list = $result->row();
		$title                  = $list->title;
		$page_viewed            = $list->page_viewed;
		
		$page_viewed = $this->Trips_model->update_pageViewed($room_id, $page_viewed);
		
			
		$id                     = $room_id;
		$checkin                = $this->session->userdata('Vcheckin');
		$checkout               = $this->session->userdata('Vcheckout');
		$guests         = $this->session->userdata('Vnumber_of_guests');
	
		$ckin                   = explode('/', $checkin);
		$ckout                  = explode('/', $checkout);
		
		//check admin premium condition and apply so for
		$query                  = $this->Common_model->getTableData( 'paymode', array('id' => 2));
		$row                    = $query->row();	

		
		if(($ckin[0] == "mm" && $ckout[0] == "mm") or ($ckin[0] == "" && $ckout[0] == ""))
		{
      			
			 if($Wprice == 0)
			{
				$data['Wprice']  = $price * 7;
			}
			else
			{
				$data['Wprice']  = $Wprice;
			}
			if($Mprice == 0)
			{
				$data['Mprice']  = $price * 30;
			}
			else
			{
				$data['Mprice']  = $Mprice;
			}
			
			 if($row->is_premium == 1)
					{
			    if($row->is_fixed == 1)
							{
										$fix            = $row->fixed_amount; 
										$amt            = $price + $fix;
										$commission = $fix;
										$Fprice         = $amt;
							}
							else
							{  
										$per            = $row->percentage_amount; 
										$camt           = floatval(($price * $per) / 100);
										$amt            = $price + $camt;
										$commission = $camt;
										$Fprice         = $amt;
							}
							
						if($Wprice == 0)
			{
				$data['Wprice']  = $price * 7;
			}
			else
			{
				$data['Wprice']  = $Wprice;
			}
			if($Mprice == 0)
			{
				$data['Mprice']  = $price * 30;
			}
			else
			{
				$data['Mprice']  = $Mprice;
			}
		
		   }
			} 
		else
		{	
			$diff                  = strtotime($ckout[2].'-'.$ckout[0].'-'.$ckout[1]) - strtotime($ckin[2].'-'.$ckin[0].'-'.$ckin[1]);
			$days                  = ceil($diff/(3600*24));
			
			if($guests > $guests)
			{
			  $price               = ($price * $days) + ($days * $xprice->addguests);
			}
			else
			{
			  $price               = $price * $days;
			}
					
			if($Wprice == 0)
			{
				$data['Wprice']  = $price * 7;
			}
			else
			{
				$data['Wprice']  = $Wprice;
			}
			if($Mprice == 0)
			{
				$data['Mprice']  = $price * 30;
			}
			else
			{
				$data['Mprice']  = $Mprice;
			}
			
			$commission    = 0;
			
			 if($row->is_premium == 1)
					{
			    if($row->is_fixed == 1)
							{
										$fix             = $row->fixed_amount; 
										$amt             = $price + $fix;
										$commission = $fix;
										$Fprice          = $amt;
							}
							else
							{  
										$per             = $row->percentage_amount; 
										$camt            = floatval(($price * $per) / 100);
										$amt             = $price + $camt;
										$commission = $camt;
										$Fprice          = $amt;
							}
							
						if($Wprice == 0)
			{
				$data['Wprice']  = $price * 7;
			}
			else
			{
				$data['Wprice']  = $Wprice;
			}
			if($Mprice == 0)
			{
				$data['Mprice']  = $price * 30;
			}
			else
			{
				$data['Mprice']  = $Mprice;
			}
		
		   }
					}
		
			$conditions              = array('list_id' => $room_id);
			$condition    = array("is_featured" => 1);
						$list_image   = $this->Gallery->get_imagesG($room_id, $condition)->row();

					if(isset($list_image->name))
					{
						$image_url_ex = explode('.',$list_image->name);

						$image = base_url().'images/'.$room_id.'/'.$image_url_ex[0].'_crop.jpg';
					}
					else
					{
						$image = base_url().'images/no_image.jpg';
					}
			
			$conditions    			        = array('list_id' => $room_id, 'userto' => $list->user_id);
			$result			     	  = $this->Trips_model->get_review($conditions);
			
			$conditions    			     	  = array('list_id' => $room_id, 'userto' => $list->user_id);
			$stars			        	= $this->Trips_model->get_review_sum($conditions)->row();	
			 
			$title            = substr($title, 0, 70);
			
			
		   $search=array('\'','"','(',')','!','{','[','}',']');
			$replace=array('&sq','&dq','&obr','&cbr','&ex','&obs','&oabr','&cbs','&cabr');
		    $desc_replace = str_replace($search, $replace, $desc);
			$desc_tags = stripslashes($desc_replace);
							 
			$amenities = $this->db->get_where('list', array('id' => $room_id))->row()->amenities;
				 $property_type = $this->db->get_where('property_type', array('id' => $property_id))->row()->type;
    $in_arr = explode(',', $amenities);
	$result = $this->db->get('amnities');				 
							 $user_name = $this->db->where('id',$user_id)->select('username')->from('users')->get();
							 if($user_name->num_rows()!=0)
							 {
							 	foreach($user_name->result() as $row)
								{
									$hoster_name = $row->username;
								}
							 }
							 else
							 	{
							 		$hoster_name = 'No Username';
							 	}
							
				 $currency1 = $this->input->get('currency');
					
					$Mprice = $Mprice;
					$price = $price;
					$extra_guest_fee = $after_guest_fee;
					$Wprice = $Wprice;
					$sublet_price = $sublet_price;
					$cleaning_fee = $cleaning_fee;
                 /*$price = $this->get_currency_value1($room_id, $price, $currency1);	
				 $Mprice = $this->get_currency_value1($room_id, $Mprice, $currency1);
				 $extra_guest_fee = $this->get_currency_value1($room_id, $after_guest_fee, $currency1);
				 $Wprice = $this->get_currency_value1($room_id, $Wprice, $currency1);
				 $sublet_price = $this->get_currency_value1($room_id, $sublet_price, $currency1);
				 $cleaning_fee = $this->get_currency_value1($room_id, $cleaning_fee, $currency1);	*/	
								
            echo "[ { \"id\":".$room_id.",\"user_id\":".$user_id.",\"hoster_name\":\"".$hoster_name."\",\"title\":\"".$title."\",\"country\":\"".$country.
			    "\",\"city\":\"".$city."\",\"state\":\"".$state."\",\"cancellation_policy\":\"".$cancellation_policy.
	           "\",\"address\":\"".$address."\",\"image_url\":\"".$image."\",
	           \"room_type\":\"".$room_type."\",\"bedrooms\":".$bedrooms.",\"bathrooms\":".$bathrooms.",\"bed_type\":\"".$bed_type."\",\"beds\":\"".$beds."\",
	           \"desc\":\"".$desc_tags."\",\"capacity\":".$capacity.",\"price\":\"".$price.
	           "\",\"cleaning_fee\":\"".$cleaning_fee."\",\"extra_guest_fee\":\"".$extra_guest_fee."\",\"weekly_price\":\"".$Wprice.
	           "\",\"monthly_price\":\"".$Mprice."\",\"email\":\"".$email."\",\"phone\":\"".$phone."\",\"review\":\"".$review.
	           "\",\"lat\":".$lat.",\"long\":".$long.",\"property_type\":\"".$property_type."\",\"street_view\":".$street_view.
	           ",\"sublet_price\":".$sublet_price.",\"sublet_status\":".$sublet_status.",\"sublet_startdate\":\"".$sublet_startdate.
	           "\",\"sublet_enddate\":\"".$sublet_enddate."\",\"currency\":\"".$currency."\",\"manual\":\"".$manual."\",\"page_viewed\":".$page_viewed
	           .",\"neighbor\":\"".$neighbor."\",\"amenities\":\"";if($result->num_rows() != 0) {
	           if($amenities)
			   {
			    foreach($result->result() as $row)
	{
	    if(in_array($row->id, $in_arr))
		{
			$json[] = $row->id.",";
		}
	}
	$count = count($json);
		  $end = $count-1;
					$slice = array_slice($json,0,$end);
					foreach($slice as $row)
					{
						echo $row; 
					}
					$comma = end($json);
					$json = substr_replace($comma ,"",-1);
					echo $json."\""; echo "} ]";exit;
			   }
			   }
else {
	$json[] ='';
	
}
		echo "\"} ]";			
			  
	}
	}
	else {
		echo '[{"status":"Access denied"}]';
	}
}

public function cancellation_policy()
{
	$result = $this->db->get('cancellation_policy');
	echo '[';
	foreach($result->result() as $row)
	{
		$cancellation_content = str_replace('<p>', '&op&', $row->cancellation_content);
		$cancellation_content = str_replace('</p>', '&cp&', $cancellation_content);
		$json[] = '{"id":"'.$row->id.'","cancellation_title":"'.$row->cancellation_title.'","cancellation_content":"'.$cancellation_content.'"},';
	}
	$count = count($json);
		  $end = $count-1;
					$slice = array_slice($json,0,$end);
					foreach($slice as $row)
					{
						echo $row; 
					}
					$comma = end($json);
					$json = substr_replace($comma ,"",-1);
					echo $json;
  	echo ']';
}

public function list_photo()
{
	$room_id = $this->input->get('room_id');
		
		$filename = dirname($_SERVER['SCRIPT_FILENAME']).'/images/'.$room_id;
		
	    if(!file_exists($filename)) 
		{
     	mkdir(dirname($_SERVER['SCRIPT_FILENAME']).'/images/'.$room_id, 0777, true);
		}
		
	  $file_element_name = "uploadedfile";
			
	  $config['upload_path'] = "./images/{$room_id}";
      $config['allowed_types'] = 'gif|jpg|png';
	  $config['remove_spaces'] = TRUE;
      $config['max_size']  = 102400 * 8;
      $config['encrypt_name'] = FALSE;
 
      $this->load->library('upload', $config);
 
      if (!$this->upload->do_upload($file_element_name))
      {
      	echo $this->upload->display_errors();
      }
      else
      {
      	 	    $upload_data = $this->upload->data(); 				
			      $image_name    = $upload_data['file_name'];
				        $insertData['list_id']    = $room_id;
      	                $insertData['name']       = $image_name;
						$insertData['created']    = local_to_gmt();
						
						$check = $this->db->where('list_id',$room_id)->get('list_photo');
						
						$photo_status['photo'] = 1;
			            $this->db->where('id',$room_id)->update('lys_status',$photo_status);
						
						if($check->num_rows() == 0)
						{
							$insertData['is_featured'] = 1;
				     	}
                        else 
                        {
	                       $insertData['is_featured'] = 0;
                        }
						if($image_name != '')
						$this->Common_model->insertData('list_photo', $insertData);
						//$this->watermark($room_id,$image_name);
						
      	  $list_photo = $this->db->where('list_id',$room_id)->order_by('id','asc')->get('list_photo');
	
   	if($list_photo->num_rows() != 0)
	{
   	foreach($list_photo->result() as $row)
     {
     	    
      $data['image_url'] = base_url()."images/".$room_id."/".$row->name;

     }
	}
 $this->watermark($room_id,$image_name);  
 $this->watermark1($room_id,$image_name); 
 
 echo json_encode($data);exit;
	  }
}

function watermark($list_id,$image_name)
	{
   $image_path =  dirname($_SERVER['SCRIPT_FILENAME']);
  
  $main_imgc		= $image_path."/images/$list_id/$image_name"; // main big photo / picture
  
// using the function to crop an image
$source_image = $main_imgc;
$main_img_ext = explode('.', $image_name);
$main_imgc = $image_path."/images/$list_id/$main_img_ext[0]";
$target_image = $main_imgc.'_crop.jpg';

$return = $this->resize_image($source_image, $target_image); 

if($return != 1)
{
	exit;
}
$main_img = $target_image;
$logo = $this->db->get_where('settings', array('code' => 'SITE_LOGO'))->row()->string_value;
$watermark_img	= $image_path."/logo/$logo"; // use GIF or PNG, JPEG has no tranparency support
$padding 		= 3;     // distance to border in pixels for watermark image
$opacity		= 50;	// image opacity for transparent watermark

$watermark 	    = imagecreatefrompng($watermark_img); // create watermark
$image_water 	= imagecreatefromjpeg($main_img); // create main graphic

if(!$image_water || !$watermark) die("Error: main image or watermark could not be loaded!");


$watermark_size 	= getimagesize($watermark_img);
$watermark_width 	= $watermark_size[0];  
$watermark_height 	= $watermark_size[1]; 

$image_size 	= getimagesize($main_img);  
$dest_x 		= $image_size[0] - $watermark_width - $padding;  
$dest_y 		= $image_size[1] - $watermark_height - $padding;


// copy watermark on main image
imagecopy($image_water, $watermark, $dest_x, $dest_y, 0, 0, $watermark_width, $watermark_height);


// print image to screen
//header("content-type: image/jpeg");   
imagejpeg($image_water,$main_imgc.'.'.$main_img_ext[1].'_watermark.jpg',100);  
//imagejpeg($image_water); 
imagedestroy($image_water);  
imagedestroy($watermark); 
return true;

	 } 

function watermark1($list_id,$image_name)
	{
   $image_path =  dirname($_SERVER['SCRIPT_FILENAME']);
  
  $main_imgc		= $image_path."/images/$list_id/$image_name"; // main big photo / picture
  
$watermark_size 	= getimagesize($main_imgc);
$watermark_width 	= $watermark_size[0];  
$watermark_height 	= $watermark_size[1]; 

$config['image_library'] = 'gd2';
$config['source_image'] = $main_imgc;
$config['new_image'] = $image_path."/images/$list_id/$image_name"."_home.jpg";
$config['maintain_ratio'] = TRUE;
$config['width'] = 1355;
$config['height'] = 500;

$this->load->library('image_lib');
$this->image_lib->initialize($config);

if ( ! $this->image_lib->resize())
{
    echo $this->image_lib->display_errors();exit;
}
else {
	//echo 'resized';exit;
}
// using the function to crop an image
$main_img = $config['new_image'];
$watermark_img	= $image_path."/images/banner_black_watermark_right.png"; // use GIF or PNG, JPEG has no tranparency support
$padding 		= 0;     // distance to border in pixels for watermark image
$opacity		= 50;	// image opacity for transparent watermark

$watermark 	    = imagecreatefrompng($watermark_img); // create watermark
$image_water 	= imagecreatefromjpeg($main_img); // create main graphic

if(!$image_water || !$watermark) die("Error: main image or watermark could not be loaded!");


$watermark_size 	= getimagesize($watermark_img);
$watermark_width 	= $watermark_size[0];  
$watermark_height 	= $watermark_size[1]; 

$image_size 	= getimagesize($main_img);  
$dest_x 		= $image_size[0] - $watermark_width - $padding;  
$dest_y 		= $image_size[1] - $watermark_height - $padding;

//echo $image_size[0].' - '.$watermark_width.'-'.$dest_x;
// copy watermark on main image
imagecopy($image_water, $watermark, $dest_x, $dest_y, 0, 0, $watermark_width, $watermark_height);


// print image to screen
//header("content-type: image/jpeg");   
imagejpeg($image_water,$main_img.'_watermark.jpg',100);  
//imagejpeg($image_water); 
imagedestroy($image_water);  
imagedestroy($watermark); 

$main_img = $main_img.'_watermark.jpg';
$watermark_img	= $image_path."/images/banner_black_watermark_left.png"; // use GIF or PNG, JPEG has no tranparency support
$padding 		= 0;     // distance to border in pixels for watermark image
$opacity		= 50;	// image opacity for transparent watermark

$watermark 	    = imagecreatefrompng($watermark_img); // create watermark
$image_water 	= imagecreatefromjpeg($main_img); // create main graphic

if(!$image_water || !$watermark) die("Error: main image or watermark could not be loaded!");


$watermark_size 	= getimagesize($watermark_img);
$watermark_width 	= $watermark_size[0];  
$watermark_height 	= $watermark_size[1]; 

$image_size 	= getimagesize($main_img);  
$dest_x 		= $image_size[0] - $watermark_width - $padding;  
$dest_y 		= $image_size[1] - $watermark_height - $padding;

//echo $image_size[0].' - '.$watermark_width.'-'.$dest_x;
// copy watermark on main image
imagecopy($image_water, $watermark, 0, $dest_y, 0, 0, $watermark_width, $watermark_height);


// print image to screen
//header("content-type: image/jpeg");   
imagejpeg($image_water,$main_img,100);  
//imagejpeg($image_water); 
imagedestroy($image_water);  
imagedestroy($watermark); 

return true;

	 } 

	/**
 * Resize Image
 *
 * Takes the source image and resizes it to the specified width & height or proportionally if crop is off.
 * @access public
 * @author Jay Zawrotny <jayzawrotny@gmail.com>
 * @license Do whatever you want with it.
 * @param string $source_image The location to the original raw image.
 * @param string $destination_filename The location to save the new image.
 * @param int $width The desired width of the new image
 * @param int $height The desired height of the new image.
 * @param int $quality The quality of the JPG to produce 1 - 100
 * @param bool $crop Whether to crop the image or not. It always crops from the center.
 */
function resize_image($source_image, $destination_filename, $width = 400, $height = 350, $quality = 70, $crop = true)
{

        if( ! $image_data = getimagesize( $source_image ) )
        {
                return false;
        }

        switch( $image_data['mime'] )
        {
                case 'image/gif':
                        $get_func = 'imagecreatefromgif';
                        $suffix = ".gif";
                break;
                case 'image/jpeg';
                        $get_func = 'imagecreatefromjpeg';
                        $suffix = ".jpg";
                break;
                case 'image/png':
                        $get_func = 'imagecreatefrompng';
                        $suffix = ".png";
                break;
        }

        $img_original = call_user_func( $get_func, $source_image );
        $old_width = $image_data[0];
        $old_height = $image_data[1];
        $new_width = $width;
        $new_height = $height;
        $src_x = 0;
        $src_y = 0;
        $current_ratio = round( $old_width / $old_height, 2 );
        $desired_ratio_after = round( $width / $height, 2 );
        $desired_ratio_before = round( $height / $width, 2 );

      //  if( $old_width < $width || $old_height < $height )
      //  {
                /**
                 * The desired image size is bigger than the original image. 
                 * Best not to do anything at all really.
                 */
      //          return false;
     //   }


        /**
         * If the crop option is left on, it will take an image and best fit it
         * so it will always come out the exact specified size.
         */
        if( $crop )
        {
                /**
                 * create empty image of the specified size
                 */
                $new_image = imagecreatetruecolor( $width, $height );

                /**
                 * Landscape Image
                 */
                if( $current_ratio > $desired_ratio_after )
                {
                        $new_width = $old_width * $height / $old_height;
                }

                /**
                 * Nearly square ratio image.
                 */
                if( $current_ratio > $desired_ratio_before && $current_ratio < $desired_ratio_after )
                {
                        if( $old_width > $old_height )
                        {
                                $new_height = max( $width, $height );
                                $new_width = $old_width * $new_height / $old_height;
                        }
                        else
                        {
                                $new_height = $old_height * $width / $old_width;
                        }
                }

                /**
                 * Portrait sized image
                 */
                if( $current_ratio < $desired_ratio_before  )
                {
                        $new_height = $old_height * $width / $old_width;
                }

                /**
                 * Find out the ratio of the original photo to it's new, thumbnail-based size
                 * for both the width and the height. It's used to find out where to crop.
                 */
                $width_ratio = $old_width / $new_width;
                $height_ratio = $old_height / $new_height;

                /**
                 * Calculate where to crop based on the center of the image
                 */
                $src_x = floor( ( ( $new_width - $width ) / 2 ) * $width_ratio );
                $src_y = round( ( ( $new_height - $height ) / 2 ) * $height_ratio );
        }
        /**
         * Don't crop the image, just resize it proportionally
         */
        else
        {
                if( $old_width > $old_height )
                {
                        $ratio = max( $old_width, $old_height ) / max( $width, $height );
                }else{
                        $ratio = max( $old_width, $old_height ) / min( $width, $height );
                }

                $new_width = $old_width / $ratio;
                $new_height = $old_height / $ratio;

                $new_image = imagecreatetruecolor( $new_width, $new_height );
        }

        /**
         * Where all the real magic happens
         */
        imagecopyresampled( $new_image, $img_original, 0, 0, $src_x, $src_y, $new_width, $new_height, $old_width, $old_height );

        /**
         * Save it as a JPG File with our $destination_filename param.
         */
          /*  $image_path =  dirname($_SERVER['SCRIPT_FILENAME']);
  
  $destination_filename		= $image_path."/images/85/crop";*/
        imagejpeg( $new_image, $destination_filename, $quality  );

        /**
         * Destroy the evidence!
         */
        imagedestroy( $new_image );
        imagedestroy( $img_original );

        /**
         * Return true because it worked and we're happy. Let the dancing commence!
         */
        return true;
} 
public function viewlist()
{
	$user_id=$this->input->get('user_id');
	
	$query=$this->db->where('user_id',$user_id)->get('list');
	if($query->num_rows()!=0)
	{
	foreach($query->result() as $row)
	{
		$data['id']=$row->id;
		$data['user_id']=$row->user_id;
		$data['address']=$row->address;
		$data['country']=$row->country;
		$data['street_address']=$row->street_address;
		$data['city']=$row->city;
		$data['state']=$row->state;
		$data['zip_code']=$row->zip_code;
		$data['lat']=$row->lat;
		$data['long']=$row->long;
		$data['property_id']=$row->property_id;
		$data['room_type']=$row->room_type;
		$data['bedrooms']=$row->bedrooms;
				$data['beds']=$row->beds;
				$data['bathrooms']=$row->bathrooms;
				$data['amenities']=$row->amenities;
				$data['title']=$row->title;
				$data['desc']=$row->desc;
				$data['capacity']=$row->capacity;
				$data['price']=$row->price;
				$data['currency']=$row->currency;
					$data['currency_symbol']=$this->db->where('currency_code',$data['currency'])->get('currency')->row('currency_symbol');
				$country_symbol =$this->db->where('currency_code',$data['currency'])->get('currency')->row('country_symbol');
				if(!empty($country_symbol)){
					$data['country_symbol']   = $country_symbol;
				}
                else{
	                $check_default  = $this->db->where('default = 1')->get('currency')->row('currency_symbol');
					//print_r($this->db->last_query());exit;
	                 $data['country_symbol']  = $check_default;
                }
				$data['home_type']=$row->home_type;
				$final[]=$data;	
				
		
	}
	echo json_encode($final);
	}
	else {
		{
			echo '[{"Status":"No Listings Found"}]';
		}
	}
	
}

public function your_listing(){
	$user_id   = $this->input->get('user_id');
	$query = $this->db->where('user_id',$user_id)->get('list');
	
	if($query->num_rows()!=0)
	{
	foreach($query->result() as $row)
	{
		$data['id']=$row->id;
		$data['user_id']=$row->user_id;
		$status_details = $row->status;
		$phtoto_status =$this->db->where('list_id',$data['id'])->get('list_photo')->row('image');
		if($status_details == 1)
		{
			$data['status']= 1 ;
			
		}
		else {
	$data['status']= 0 ;
			}
		
		
	    $data['country']=$row->country;
		 
		$data['room_type']=$row->room_type;
				$data['title']=$row->title;
				$data['desc']=$row->desc;
				$data['image'] = $phtoto_status;
				$listing[]=$data;	
				
	}
	echo json_encode($listing);
	}
	else {
	echo '[{"reason_message":"No data found"}]';
}
}

public function other(){
	$query = $this->db->query('select * from property_type');
	foreach($query->result() as $row)
	{
	$id = $row->id;
	if($id > 3){
		$data['id'] = $row->id;
		$data['type'] = $row->type;
		$other[] =$data;
	}
	}
	echo json_encode($other);
	
}

public function display_amnities()
{
	$amnities = $this->db->query('select * from amnities');
	foreach ($amnities->result() as $row) {
		$data['id'] = $row->id;
		$data['name'] = $row->name;
		$data['description'] = $row->description;
		$amenities[] = $data;
	}
	echo json_encode($amenities);
}
	
public function amnities(){
	$roomid = $this->input->get('roomid');
	$query = $this->db->where('id',$roomid)->get('list');
	if($query->num_rows()!=0)
	{
	foreach($query->result() as $row)
	{
		$data['id']=$row->id;
		$data['amenities']=$row->amenities;
		$select[] = $data;
	}
	echo json_encode($select);
	}
}

public function edit_amnities(){
	$this->load->model('common_model');
		$roomid=$this->input->get('roomid');
	
		$data['amenities']			 = $this->input->get("amenities");
		$level = explode(',', $data['amenities']);
		if($roomid == ''){
		  echo '[{"status":"You should provide roomid."}]';exit;
		}
		else{
		$this->db->where('id',$roomid)->update('list',$data);
		
		echo '[{"reason_message":"Updated Successfully"}]';
		}
}
public function preview(){
	$roomid = $this->input->get('roomid');
	$query = $this->db->where('id',$roomid)->get('list');
	if($query->num_rows()!=0)
	{
	foreach($query->result() as $row)
	{
		$data['id']=$row->id;
		$data['user_id'] = $row->user_id;
		$data['country']=$row->country;
		$data['address'] = $row->address;
		$data['room_type']=$row->room_type;
		$data['bedrooms']=$row->bedrooms;
				$data['beds']=$row->beds;
				$data['bathrooms']=$row->bathrooms;
				$data['bed_type']=$row->bed_type;
				$data['amenities']=$row->amenities;
				$data['title']=$row->title;
				$data['desc']=$row->desc;
				$data['price']=$row->price;
				$data['capacity']=$row->capacity;
				$data['image']=$this->db->where('list_id',$data['id'])->get('list_photo')->row('image');
				$data['map']=$this->db->where('list_id',$data['id'])->get('map_photo')->row('map');
				$data['week']=$this->db->where('id',$data['id'])->get('price')->row('week');
				$data['month']=$this->db->where('id',$data['id'])->get('price')->row('month');
				$data['Fname']=$this->db->where('id',$data['user_id'])->get('profiles')->row('Fname');
				$data['email']=$this->db->where('id',$data['user_id'])->get('profiles')->row('email');
				$data['src']=$this->db->where('email',$data['email'])->get('profile_picture')->row('src');
			$currency  = $row->currency;
				$data['currency_symbol']  = $this->db->where('currency_code',$currency)->get('currency')->row('currency_symbol');
				$country_symbol  = $this->db->where('currency_code',$currency)->get('currency')->row('country_symbol');
				$country_code  = $this->db->where('currency_code',$currency)->get('currency')->row('currency_code');
				if(!empty($country_symbol)){
					$data['country_symbol']   = $country_symbol;
				}
                else{
                	$check_default  = $this->db->where('default = 1')->get('currency')->row('currency_symbol');
					//print_r($this->db->last_query());exit;
	                 $data['country_symbol']  = $check_default;
                }
				if(!empty($country_code)){
					$data['currency_code']   = $country_code;
				}
                else{
                	$check_default1  = $this->db->where('default = 1')->get('currency')->row('currency_code');
					//print_r($this->db->last_query());exit;
	                 $data['currency_code']  = $check_default1;
                }
				//$data['currency_symbol']  = $this->db->where('currency_code',$currency)->get('currency')->row('currency_symbol');
						$prev[]=$data;	
				
		
	}
	echo json_encode($prev);
	}
}

public function apartment_detail(){
	$roomid = $this->input->get('roomid');
	$query = $this->db->where('id',$roomid)->get('list');
	if($query->num_rows()!=0)
	{
	foreach($query->result() as $row)
	{
		$data['id']=$row->id;
		$data['user_id'] = $row->user_id;
		$data['country']=$row->country;
		$data['state']  = $row->state;
		$data['address'] = $row->address;
		$data['city']  = $row->city;
		$data['room_type']=$row->room_type;
		$data['bedrooms']=$row->bedrooms;
				$data['beds']=$row->beds;
				$data['bathrooms']=$row->bathrooms;
				$data['bed_type']=$row->bed_type;
				$data['amenities']=$row->amenities;
				$data['title']=$row->title;
				$data['desc']=$row->desc;
				$data['price']=$row->price;
				$data['capacity']=$row->capacity;
				$data['week']=$this->db->where('id',$data['id'])->get('price')->row('week');
				$data['month']=$this->db->where('id',$data['id'])->get('price')->row('month');
				$data['Fname']=$this->db->where('id',$data['user_id'])->get('profiles')->row('Fname');
				$data['email']=$this->db->where('id',$data['user_id'])->get('profiles')->row('email');
				$data['src']=$this->db->where('email',$data['email'])->get('profile_picture')->row('src');
				$currency  = $row->currency;
				$data['currency_symbol']  = $this->db->where('currency_code',$currency)->get('currency')->row('currency_symbol');
					$country_symbol  = $this->db->where('currency_code',$currency)->get('currency')->row('country_symbol');
				$country_code  = $this->db->where('currency_code',$currency)->get('currency')->row('currency_code');
				if(!empty($country_symbol)){
					$data['country_symbol']   = $country_symbol;
				}
                else{
	                $check_default  = $this->db->where('default = 1')->get('currency')->row('currency_symbol');
					//print_r($this->db->last_query());exit;
	                 $data['country_symbol']  = $check_default;
                }
				if(!empty($country_code)){
					$data['currency_code']   = $country_code;
				}
                else{
                	$check_default1  = $this->db->where('default = 1')->get('currency')->row('currency_code');
					//print_r($this->db->last_query());exit;
	                 $data['currency_code']  = $check_default1;
                }
			    	//$data['list_id'] = $this->db->where('list_id',$roomid)->get('list_photo')->result_array();
			    
				$data['image']=$this->db->where('list_id',$data['id'])->get('list_photo')->row('image');
				$data['map']=$this->db->where('list_id',$data['id'])->get('map_photo')->row('map');
				$apart[]=$data;	
				
		
	}
	echo json_encode($apart);
	}
}

}	 
?>