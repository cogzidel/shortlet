<?php
/**
 * DROPinn Search Controller Class
 *
 * helps to achieve common tasks related to the site for mobile app like android and iphone.
 *
 * @package		Dropinn
 * @subpackage	Controllers
 * @category	Search
 * @author		Cogzidel Product Team
 * @version		Version 1.0
 * @link		http://www.cogzidel.com
 
 */
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search extends CI_Controller {

	public function Search()
	{
		parent::__construct();
		
		$this->load->helper('url');
		
		$this->load->library('DX_Auth');  

		$this->load->model('Users_model');
		$this->load->model('Gallery');
	}
	
	public function index()
	{
  //Get the checkin and chekout dates
  $checkin           = '';
		$checkout          = ''; 
		$stack             = array();
		$room_types        = array();
		$property_type_id  = array();
		$checkin           = $this->input->get('checkin');   
		$checkout          = $this->input->get('checkout');
		$nof_guest         = $this->input->get('guests');
		$room_types        = $this->input->get('room_types');
		$search_view       = $this->input->get('search_view');
		
		$min               = $this->input->get('price_min');
		$max               = $this->input->get('price_max');
		
		$keywords          = $this->input->get('keywords');
		
	 $search_by_map     = $this->input->get('search_by_map');
		$sw_lat            = $this->input->get('sw_lat');
		$sw_lng            = $this->input->get('sw_lng');
		$ne_lat            = $this->input->get('ne_lat');
		$ne_lng            = $this->input->get('ne_lng');
		
		$min_bedrooms      = $this->input->get('min_bedrooms');
		$min_bathrooms     = $this->input->get('min_bathrooms');
		$min_beds          = $this->input->get('min_beds');
		
		$property_type_id  = $this->input->get('property_type_id');
		$hosting_amenities = $this->input->get('hosting_amenities');
		
		
		$array_items = array(
												'Vcheckin'                => '',
												'Vcheckout'               => '',
												'Vcheckout'					          => '',
								);
    $this->session->unset_userdata($array_items);
				
			if($this->input->post('checkin') != '' || $this->input->post('checkin') != 'mm/dd/yy')
		 {
		 	$freshdata = array(
									'Vcheckin'                => $this->input->get('checkin'),
									'Vcheckout'               => $this->input->get('checkout'),
									'Vnumber_of_guests'					  => $this->input->get('number_of_guests'),
					);
			 $this->session->set_userdata($freshdata);
				}
		
		 if($checkin!='--' && $checkout!='--' && $checkin!="yy-mm-dd" && $checkout!="yy-mm-dd" )
		 { 
						$ans = $this->db->query("SELECT id,list_id FROM `calendar` WHERE `booked_days` = '".$checkin."' OR `booked_days` = '".$checkout."' GROUP BY `list_id`");
						//echo $this->db->last_query();exit;
						$a   = $ans->result();
						$this->db->flush_cache();
						// Now after the checkin is completed
						if(!empty($a))
						{
							foreach($a as $a1)
							{ 
								array_push($stack, $a1->list_id);
							}
						}	
		 }
		  
		$query  = $this->input->get('location');
		$pieces = explode(",", $query);

		$print  = "";
		$len    = count($pieces);
		
	 	$condition = ''; 
	 	$condition .= "(`status` != '0')";
		
		if($search_by_map)
		{
		$condition .= "AND (`lat` BETWEEN $sw_lat AND $ne_lat) AND (`long` BETWEEN $sw_lng AND $ne_lng)";
		}
		else
		{
		if($query != '')
		{
			$i = 1;
			foreach($pieces as $address)
			{
				$this->db->flush_cache();		
				$address = $this->db->escape_like_str($address);
				
				if($i == $len)
				$and = "";
				else
				$and = " OR ";

				if($i == 1)
				$condition .= " AND (";
				
				$condition .=  "`address`  LIKE '%".$address."%' OR `country` LIKE '%".$address."%'".$and;
				
				if($i == $len)
				$condition .= ")";
				
				$i++;
			}
		}
		}
		
		if(!empty($min_bedrooms))
		{
				$condition .= " AND (`bedrooms` = '".$min_bedrooms."')";
		}
		if(!empty($property_type))
		{
				$condition .= " AND (`property_id` = '".$property_type."')";
		}
		if(!empty($min_bathrooms))
		{
				$condition .= " AND (`bathrooms` = '".$min_bathrooms."')";
		}
		
		if(!empty($min_beds))
		{
		  $condition .= " AND (`beds` = '".$min_beds."')";
		}
	
		if(!empty($stack))
		{ 
			$condition .= " AND (`id` NOT IN(".implode(',',$stack)."))";
		}
		
		if($nof_guest > 1)
		{
			$condition .= " AND (`capacity` >= '".$nof_guest."')";
		}
		
		
	if(is_array($room_types))
		{
			if(count($room_types) > 0)
			{
			    $i = 1;
							foreach($room_types as $room_type)
							{							
									if($i == count($room_types))
									$and = "";
									else
									$and = " AND ";
									$or=" OR ";
					
									if($i == 1)
									$condition .= " AND (";
									
									$condition .=  "`room_type` LIKE '%".$room_type."%'".$or."`neighbor` = '".$room_type."'".$or."`neighbor` = '".$room_type."'".$or."`room_type` = '".$room_type."'".$or."`room_type` = '".$room_type."'".$and;									
									
									if($i == count($room_types))
									$condition .= ")";
									
									$i++;
							}
			
			}
		}	

				if(is_array($hosting_amenities))
				{
					if(count($hosting_amenities) > 0)
					{
					    $i = 1;
					    foreach($hosting_amenities as $amenity)
     				{
												if($i == count($hosting_amenities))
												$and = "";
												else
												$and = " AND ";
								
												if($i == 1)
												$condition .= " AND (";
												
												$condition .=  "`amenities`  LIKE '%".$amenity."%'".$and;
												
												if($i == count($hosting_amenities))
												$condition .= ")";
												
												$i++;
     				}
					
					}
				}	
				
					
			if(isset($min))
			{
				if($min > 0)
				{
						$this->db->where('price >=', $min);
				}
			}
			else
			{
					if(isset($max))
					{
					$min = 0;
					}
			}
			
			if(isset($max))
			{
					if($max > $min)
					{
						$this->db->where('price <=', $max);
					}
			}
			

			if(is_array($property_type_id))
			{
				if(count($property_type_id) > 0)
				{   $i = 1;
								foreach($property_type_id as $property_id)
								{ 
									if($i == count($property_type_id))
									{
									$and = "";
									}
									else
									{
									$and = " OR ";
									}
					
									if($i == 1)
									$condition .= " AND (";
									
									$condition .=  "`property_id` = '".$property_id."'".$and;
									
									if($i == count($property_type_id))
									$condition .= ")";
												
									$i++;
								}
				
				}
			}		
			
			if(!empty($keywords))
			{
			  $keywords = $this->db->escape_like_str($keywords);
					
					$condition .= " AND (`address`  LIKE '%".$keywords."%' OR  `title`  LIKE '%".$keywords."%' OR  `desc`  LIKE '%".$keywords."%')";
			}
		
   			//Exececute the query
   			
		$condition .= " AND (`status` != '0') AND (`user_id` != '0') AND (`address` != '0') AND (`is_enable` = '1')";
		
		$query_status = $this->db->where($condition)->get('list');
		
			  if($query_status->num_rows() != 0)
			{
				foreach($query_status->result() as $row_status)
				{
					$result_status = $this->db->where('id',$row_status->id)->get('lys_status');
					
					if($result_status->num_rows() != 0)
					{
						$result_status = $result_status->row();
											
				        $total = $result_status->calendar+$result_status->price+$result_status->overview+$result_status->photo+$result_status->address+$result_status->listing;
	
						if($total != 6)
						{
							$condition .= " AND (`id` != '".$row_status->id."')";
						}
					}
				}
			}	
			
			//$data['query'] = $this->db->get('list');
			$data['query'] = $this->db->query("SELECT * FROM (`list`) WHERE $condition");
			$tCount        = $data['query']->num_rows();
			//echo $this->db->last_query();exit;
			
			$properties = '';
			$sno   = 1; 
			if($data['query']->num_rows() > 0)
			{
					foreach($data['query']->result() as $row)
					{
						 $currency_symbol = $this->db->select('currency_symbol')->where('currency_code',$row->currency)->get('currency')->row()->currency_symbol;
					
						 $condition    = array("is_featured" => 1);
						$list_image   = $this->Gallery->get_imagesG($row->id, $condition)->row();

					if(isset($list_image->name))
					{
						$image_url_ex = explode('.',$list_image->name);

						$url = base_url().'images/'.$row->id.'/'.$image_url_ex[0].'_crop.jpg';
					}
					else
					{
						$url = base_url().'images/no_image.jpg';
					}
					
					$profile_pic = $this->Gallery->profilepic($row->user_id, 3);
					
					if($tCount == $sno) $comma = ''; else $comma = ',';
										
					$properties .= '{
					               "available":true,
																				"user_thumbnail_url":"'.$profile_pic.'",
																				"user_is_superhost":false,
																				"lat":'.$row->lat.',
																				"has_video":false,
																				"recommendation_count":0,
																				"lng":'.$row->long.',
																				"user_id":'.$row->user_id.',
																				"user_name":"'.get_user_by_id($row->user_id)->username.'",
																				"review_count":'.$row->review.',
																				"address":"'.$row->address.'",
																				"city":"'.$row->city.'",
																				"state":"'.$row->state.'",
																				"country":"'.$row->country.'",
																				"name":"'.$row->title.'",
																				"hosting_thumbnail_url":"'.$url.'",
																				"id":'.$row->id.',
																				"price":'.$row->price.',
																				"currency":"'.$row->currency.'",
																				"currency_symbol":"'.$currency_symbol.'"
																				}'.$comma;
								
		
					$sno++;
					}
			}
			else
			{
			  $properties = '{"available":false,"reason_message":"Your search was a little too specific, searching for a different city."}';
			}
	    
	
			$ajax_result  = '[';
																				
			$ajax_result .= 	$properties;		
			
			$ajax_result .=']';													
			
			echo $ajax_result;
	}
	
	
	public function dateconvert($date)
	{
		$ckout = explode('/', $date);
		$diff = $ckout[2].'-'.$ckout[0].'-'.$ckout[1];
		return $diff;
	}
	
	public function discover(){
	$dis = $this->db->query('select * from list');
	//print_r($dis);
	foreach($dis->result() as $row){
		$id = $row->id;
		$user_id = $row->user_id;
		$email=$this->db->where('id',$user_id)->get('profiles')->row('email');
		if($id <= 3)
		{
		$data['id']  = $row->id;
		$data['image'] = $this->db->where('list_id',$id)->get('list_photo')->row('image');
		$data['title'] = $row->title;
		$data['user_id'] = '';
		$data['price']= '';
		$data['country'] = '';
		$data['city']  = '';
		$data['address']  = '';
		$data['room_type'] = '';
		$data['currency_symbol']= '';
		$data['country_symbol']= '';
		$data['src'] = '';
				
		$disc[] = $data;
		}
		else if($id > 4 && $id <= 10)
		{
		$data['id']  = $row->id;
		$data['user_id'] = $row->user_id;
		$data['title']=$row->title;
		$data['price']=$row->price;
		$data['country'] = $row->country;
		$data['city']  = $row->city;
		$data['address']  = $row->address;
		$data['room_type'] = $row->room_type;
		$currency  = $row->currency;
				$data['currency_symbol']  = $this->db->where('currency_code',$currency)->get('currency')->row('currency_symbol');
				$country_symbol  = $this->db->where('currency_code',$currency)->get('currency')->row('country_symbol');
				$country_code  = $this->db->where('currency_code',$currency)->get('currency')->row('currency_code');
				if(!empty($country_symbol)){
					$data['country_symbol']   = $country_symbol;
				}
                else{
	                $check_default  = $this->db->where('default = 1')->get('currency')->row('currency_symbol');
					//print_r($this->db->last_query());exit;
	                 $data['country_symbol']  = $check_default;
                }
				if(!empty($country_code)){
					$data['currency_code']   = $country_code;
				}
                else{
                	$check_default1  = $this->db->where('default = 1')->get('currency')->row('currency_code');
					//print_r($this->db->last_query());exit;
	                 $data['currency_code']  = $check_default1;
                }
		$data['image']=$this->db->where('list_id',$data['id'])->get('list_photo')->row('image');
		$data['src'] = $this->db->where('email',$email)->get('profile_picture')->row('src');
				
		$disc[] = $data;
		}
		
	}
	echo json_encode($disc);
}

public function default_discover(){
	$query = $this->db->query('select * from discover');
	
	foreach($query->result() as $cover){
		$data['id']  = $cover->id;
		$data['country']  = $cover->country;
		$data['image']   = $cover->image;
		$val[]  = $data;
	}
	echo json_encode($val);
}

public function filter(){
		//$roomid            = $this->input->get('roomid');
		$checkin           = $this->input->get('checkin');   
		$checkout          = $this->input->get('checkout');
		$guest         = $this->input->get('guest');
		$room_type     = $this->input->get('room_type');
		$price         = $this->input->get('price');
		$amenities         = $this->input->get('amenities');
		$beds         = $this->input->get('beds');
		$bedrooms         = $this->input->get('bedrooms');
		$bathrooms         = $this->input->get('bathrooms');
		
		$data1['checkin']    = get_gmt_time(strtotime($checkin));
		$data1['checkout']   = get_gmt_time(strtotime($checkout));
		
		$query   = $this->db->query('select * from `list` where `capacity` = "'.$guest.'" AND `room_type` = "'.$room_type.'" AND `price` = "'.$price.'" AND `amenities` = "'.$amenities.'" AND `beds` = "'.$beds.'" AND `bedrooms` = "'.$bedrooms.'" AND `bathrooms` = "'.$bathrooms.'"');
        //print_r($this->db->last_query());
		if($query->num_rows()!=0)
	    {
	     foreach($query->result() as $row)
	     {
	     	$data['id']  = $row->id;
			$data['user_id']=$row->user_id;
		$data['country']=$row->country;
		$data['city']=$row->city;
		$data['state']=$row->state;
		$data['room_type']=$row->room_type;
		$data['email']=$this->db->where('id',$data['user_id'])->get('profiles')->row('email');
				$data['title']=$row->title;
				$data['price']=$row->price;
				$data['capacity']=$row->capacity;
				$data['currency']=$row->currency;
				$data['currency_symbol']=$this->db->where('currency_code',$data['currency'])->get('currency')->row('currency_symbol');
				$data['image']=$this->db->where('list_id',$data['id'])->get('list_photo')->row('image');
				$data['src'] = $this->db->where('email',$data['email'])->get('profile_picture')->row('src');
				
				$fil[]=$data;
		
	     }
           echo json_encode($fil);
	    }
		else{
	echo '[{"status":"No List Found"}]';
    }
		
	}

public function listing(){
	$location = $this->input->get('location');
		
		
		$this->db->like('address',$location);
		$this->db->or_like('city',$location);
		$this->db->or_like('state',$location);
		$this->db->or_like('country',$location);
		
		$query = $this->db->where('status != 0')->get('list');
		
		
	//$query = $this->db->query('SELECT * FROM `list` LIKE `address` = "'.$location.'" OR `city` = "'.$location.'" OR `state` = "'.$location.'" OR `country` = "'.$location.'"');
	//print_r($this->db->last_query());exit;
	if($query->num_rows()!=0)
	{
	foreach($query->result() as $row)
	{
		$data['id']=$row->id;
		$data['user_id']=$row->user_id;
		$data['country']=$row->country;
		$data['list_status']=$row->status;
		$data['address']=$row->address;
		$data['city']=$row->city;
		$data['state']=$row->state;
		$data['room_type']=$row->room_type;
		$data['email']=$this->db->where('id',$data['user_id'])->get('profiles')->row('email');
		
				$data['title']=$row->title;
				$data['price']=$row->price;
				$data['capacity']=$row->capacity;
				//$data['currency']=$row->currency;
				$currency  = $row->currency;
				$country_symbol=$this->db->where('currency_code',$currency)->get('currency')->row('currency_symbol');
				$country_code  = $this->db->where('currency_code',$currency)->get('currency')->row('currency_code');
				if(!empty($country_symbol)){
					$data['country_symbol']   = $country_symbol;
				}
                else{
	                $check_default  = $this->db->where('default = 1')->get('currency')->row('currency_symbol');
					//print_r($this->db->last_query());exit;
	                 $data['country_symbol']  = $check_default;
                }
				if(!empty($country_code)){
					$data['currency_code']   = $country_code;
				}
                else{
                	$check_default1  = $this->db->where('default = 1')->get('currency')->row('currency_code');
					//print_r($this->db->last_query());exit;
	                 $data['currency_code']  = $check_default1;
                }
				
				
				$data['image']=$this->db->where('list_id',$data['id'])->get('list_photo')->row('image');
				if(!empty($data['email']))
				{
				$data['src'] = $this->db->where('email',$data['email'])->get('profile_picture')->row('src');
				}
				else
					{
						$data['src'] ='';
					}
				$data['status']  = 'List Found';
				
				$loc[]=$data;		
		//print_r($data['src']);
	}
	echo json_encode($loc);
	}
else{
	echo '[{"status":"No List Found"}]';
}
}


	

	}
	?>