<div id="View_Login">
	<?php
	//Show Flash Message
	if($msg = $this->session->flashdata('flash_message'))
	{
		$validation_msg = $msg;
		//redirect_admin('backend');
	}
	?>

<!--CONTENT-->
<div class="container-fluid top-sp body-color login-page">
	<div class="container">
		<div class="col-md-12">
		<h1 class="login-header"><?php  echo translate_admin("Member Area"); ?> - <?php echo translate_admin("Login"); ?> </h1>

        	<form method="post" action="<?php echo site_url('administrator/login'); ?>">
			<table class="table-login" cellpadding="2" cellspacing="0">
				<tr>
					<td style="padding: 20px 0px 0px 0px;">
						<?php echo translate_admin("Username"); ?><span style="color: #ff0000;">*</span>
					</td>
					<td style="padding: 20px 0px 0px 0px;">
						<input class="focus" type="text" name="usernameli" value="<?php echo set_value('usernameli'); ?>"/>
					</td>
				</tr>
						<?php echo form_error('usernameli'); ?>
                <tr>
                	<td style="padding: 20px 0px 0px 0px;">
                		<?php echo translate_admin("Password"); ?><span style="color: #ff0000;">*</span>
                	</td>
                	<td style="padding: 20px 0px 0px 0px;">
                		<input class="focus" type="password" name="passwordli" value=""/>
                	</td>
                </tr>
						<?php echo form_error('passwordli'); ?>
                <tr>
                	<td>&#160;</td>
                	<td style="padding: 20px 0px 0px 0px;">
                		<input type="submit" name="loginAdmin" value="<?php echo translate_admin("Submit"); ?>">
                        <input class="can-but" type="reset"name="reset" value="<?php echo translate_admin("Reset"); ?>">
                	</td>
                </tr>
				<tr>
					<td colspan="2">&#160;</td>
				</tr>
				<tr>
				<td colspan="2"><a class="login-txt" href="<?php echo base_url(); ?>"><?php echo translate_admin("Return to site Home Page"); ?></a></td></tr>
				</table></div>             		
		</form>
        
        <p style="color:red;"><?php 
        if(isset($validation_msg))
        echo translate_admin("Use a valid username and password to gain access to the Administrator Back-end").'.'; 
        ?></p>
        
        <div class="clsLog_Bg"></div>
        <div class="clear"></div>
</div>
<!--END OF CONTENT-->
</div>
</div>